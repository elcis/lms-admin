package com.fsoft.lms_admin.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fsoft.lms_admin.R;
import com.fsoft.lms_admin.model.Category;

import java.util.ArrayList;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {
    private ArrayList<Category> categoriesList;

    //-------------------------------------------------------------
    public static class CategoryViewHolder extends RecyclerView.ViewHolder {
        public TextView tvCategory;

        //-------------------------------------------------------------
        public CategoryViewHolder(View itemView) {
            super(itemView);
            tvCategory = itemView.findViewById(R.id.tvCategory);
        }
    }

    //-------------------------------------------------------------
    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
        CategoryViewHolder categoryViewHolder = new CategoryViewHolder(view);

        return categoryViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        Category currentCategory = categoriesList.get(position);

        holder.tvCategory.setText(currentCategory.getCategoryName());
    }

    @Override
    public int getItemCount() {
        return categoriesList.size();
    }

    //-------------------------------------------------------------
    public CategoryAdapter(ArrayList<Category> list) {
        categoriesList = list;
    }
}
