package com.fsoft.lms_admin;

import com.fsoft.lms_admin.model.Category;
import com.fsoft.lms_admin.model.Post;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface APIService {

    @GET("posts")
    Call<List<Post>> getPosts();

    @POST("posts")
    Call<Post> createPost(@Body Post post);

    //-------------------------------------------------------------
    @GET("category/all")
    Call<List<Category>> getAllRoots();

    @GET("category/get/{categoryName}")
    Call<Category> getCategory(@Path("categoryName") String categoryName);

    @POST("category/")
    Call<Category> createCategory(@Body Category category);

}
