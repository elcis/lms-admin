package com.fsoft.lms_admin.fragment.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.fsoft.lms_admin.APIService;
import com.fsoft.lms_admin.R;
import com.fsoft.lms_admin.activity.ManageCategoryActivity;
import com.fsoft.lms_admin.model.Category;

import org.jetbrains.annotations.NotNull;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddCategoryDialog extends AppCompatDialogFragment {
    private EditText edtCategoryName;
    private AddCategoryDialogListener addCategoryDialogListener;
    private ProgressDialog progressDialog;

    //-------------------------------------------------------------
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_dialog_add_category, null);

        edtCategoryName = view.findViewById(R.id.edtCategoryName);

        builder.setView(view)
                .setIcon(R.drawable.ic_add_list)
                .setTitle("Add new category")
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton("SAVE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Category newCategory = new Category(null, edtCategoryName.getText().toString(), null);
                        progressDialog = new ProgressDialog(getActivity());
                        progressDialog.setTitle("Uploading category..");
                        progressDialog.show();
                        createCategory(newCategory);
                    }
                });

        edtCategoryName = view.findViewById(R.id.edtCategoryName);

        return builder.create();
    }

    //-----------------------------------------------------------------------------------------------
    @Override
    public void onAttach(@NonNull @NotNull Context context) {
        super.onAttach(context);

        try {
            addCategoryDialogListener = (AddCategoryDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement AddCategoryDialogListener");
        }

    }

    //-----------------------------------------------------------------------------------------------
    public interface AddCategoryDialogListener {
        void sendNewCategoryToActivity(Category category);
    }

    //-----------------------------------------------------------------------------------------------
    private void createCategory(Category category) {
        //Send POST request to API server to create new category
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://backend-lms.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        APIService apiService = retrofit.create(APIService.class);

        Call<Category> call = apiService.createCategory(category);

        call.enqueue(new Callback<Category>() {
            @Override
            public void onResponse(Call<Category> call, Response<Category> response) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if (!response.isSuccessful()) {
                    System.out.println("code: " + response.code());
                    return;
                }

                Category responseCategory = response.body();
                //Send created category's information to ManageCategoryActivity
                addCategoryDialogListener.sendNewCategoryToActivity(responseCategory);
            }

            @Override
            public void onFailure(Call<Category> call, Throwable t) {
                if (progressDialog.isShowing()){
                    progressDialog.dismiss();
                }

                Toast.makeText(getActivity(), "No response received from API", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
