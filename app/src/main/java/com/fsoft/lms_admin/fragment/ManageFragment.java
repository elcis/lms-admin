package com.fsoft.lms_admin.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fsoft.lms_admin.R;
import com.fsoft.lms_admin.activity.ManageCategoryActivity;

public class ManageFragment extends Fragment {
    CardView btnCategory;
    CardView btnQuizLesson;
    CardView btnUser;
    CardView btnReport;

    //------------------------------------------------------------
    public ManageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manage, container, false);

        //Initialize buttons
        btnCategory = view.findViewById(R.id.btnCategory);
        btnQuizLesson = view.findViewById(R.id.btnQuizLesson);
        btnUser = view.findViewById(R.id.btnUser);
        btnReport = view.findViewById(R.id.btnReport);

        //Set listener to each button
        btnCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ManageCategoryActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }
}