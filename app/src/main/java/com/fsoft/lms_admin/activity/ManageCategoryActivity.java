package com.fsoft.lms_admin.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fsoft.lms_admin.APIService;
import com.fsoft.lms_admin.R;
import com.fsoft.lms_admin.adapter.CategoryAdapter;
import com.fsoft.lms_admin.fragment.dialog.AddCategoryDialog;
import com.fsoft.lms_admin.model.Category;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ManageCategoryActivity extends AppCompatActivity implements AddCategoryDialog.AddCategoryDialogListener {
    private ImageView btnBackFromActivityCategory;
    private ImageView btnAddCategory;
    private ProgressDialog progressDialog;

    private RecyclerView recyclerViewCategoriesList;
    private CategoryAdapter categoryAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private ArrayList<Category> categoriesList;

    //--------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_category);

        //Initialize all widgets ids
        btnBackFromActivityCategory = findViewById(R.id.backFromActivityCategory);
        btnAddCategory = findViewById(R.id.btnAddCategory);
        recyclerViewCategoriesList = findViewById(R.id.recyclerViewCategoriesList);

        getCategories();

        //Set up recycler view
        categoriesList = new ArrayList<>();
        recyclerViewCategoriesList.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        categoryAdapter = new CategoryAdapter(categoriesList);
        recyclerViewCategoriesList.setLayoutManager(layoutManager);
        recyclerViewCategoriesList.setAdapter(categoryAdapter);

        //Set listener to each button
        btnBackFromActivityCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManageCategoryActivity.super.onBackPressed();
            }
        });

        btnAddCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddCategoryDialog();
            }
        });
    }

    //-------------------------------------------------------------------------
    private void getCategories() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Loading data..");
        progressDialog.show();

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://backend-lms.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        APIService apiService = retrofit.create(APIService.class);

        Call<List<Category>> call = apiService.getAllRoots();

        call.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                if (progressDialog.isShowing()){
                    progressDialog.dismiss();
                }

                if (!response.isSuccessful()) {
                    System.out.println("code: " + response.code());
                    return;
                }

                List<Category> responseCategories = response.body();
                categoriesList.addAll(responseCategories);
//                categoryAdapter.notifyItemRangeInserted(0, categoriesList.size() - 1);
                categoryAdapter.notifyDataSetChanged();
                System.out.println(categoriesList.toString());
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                if (progressDialog.isShowing()){
                    progressDialog.dismiss();
                }

                Toast.makeText(ManageCategoryActivity.this, "No response received from API", Toast.LENGTH_SHORT).show();
            }
        });

    }

    //-------------------------------------------------------------------------
    private void openAddCategoryDialog() {
        AddCategoryDialog addCategoryDialog = new AddCategoryDialog();
        addCategoryDialog.show(getSupportFragmentManager(), null);
    }

    //-------------------------------------------------------------------------
    @Override
    public void sendNewCategoryToActivity(Category category) {
        Toast.makeText(this, "Uploaded category successfully", Toast.LENGTH_SHORT).show();
        categoriesList.add(category);
        categoryAdapter.notifyItemInserted(categoriesList.size() - 1);
    }
}
