package com.fsoft.lms_admin.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.fsoft.lms_admin.APIService;
import com.fsoft.lms_admin.R;
import com.fsoft.lms_admin.fragment.AdminLogFragment;
import com.fsoft.lms_admin.fragment.ManageFragment;
import com.fsoft.lms_admin.fragment.StatisticsFragment;
import com.fsoft.lms_admin.model.Category;
import com.fsoft.lms_admin.model.Post;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private BottomNavigationView navbar;

    private TextView toolbarTitle;
    private ImageView btnLogOut;

    private ManageFragment manageFragment;
    private StatisticsFragment statisticsFragment;
    private AdminLogFragment adminLogFragment;

    //---------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Disable night mode of the phone
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        //Set up log out button
        btnLogOut = findViewById(R.id.btnLogOut);
        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setIcon(R.drawable.ic_exit)
                        .setTitle("Log out")
                        .setMessage("Are you sure you want to log out?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Code Log-out here
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        //Set up toolbar
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.toolbarTitle);
        toolbarTitle.setText("Manage");

        //Set up bottom navigation bar
        navbar = findViewById(R.id.navbar);
        navbar.setItemIconTintList(null);
        navbar.setOnItemSelectedListener(navbarListener);

        //Initialize fragments
        manageFragment = new ManageFragment();
        statisticsFragment = new StatisticsFragment();
        adminLogFragment = new AdminLogFragment();

        //Set default fragment
        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, manageFragment).commit();

//        //---------------------------------------test api------------------------------------------------------------
//        TextView tvTestOnly = findViewById(R.id.tvTestOnly);
//
//        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
//        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//
//        OkHttpClient okHttpClient = new OkHttpClient.Builder()
//                .addInterceptor(loggingInterceptor)
//                .build();
//
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("https://backend-lms.herokuapp.com/")
//                .addConverterFactory(GsonConverterFactory.create())
//                .client(okHttpClient)
//                .build();
//
//        APIService apiService = retrofit.create(APIService.class);

//      GET ALL PARENTS
//        Call<List<Category>> call = apiService.getAllParents();
//
//        call.enqueue(new Callback<List<Category>>() {
//            @Override
//            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
//                if (!response.isSuccessful()){
//                    tvTestOnly.setText("Code: " + response.code());
//                    return;
//                }
//
//                List<Category> categoryList = response.body();
//                for (Category category: categoryList)
//                    tvTestOnly.append(category.toString());
//
//            }
//
//            @Override
//            public void onFailure(Call<List<Category>> call, Throwable t) {
//
//            }
//        });

//        GET CATEGORY BY NAME
//        Call<Category> call = apiService.getCategory("parent");
//        call.enqueue(new Callback<Category>() {
//            @Override
//            public void onResponse(Call<Category> call, Response<Category> response) {
//                if (!response.isSuccessful()) {
//                    System.out.println("Code: " + response.code());
//                    return;
//                }
//
//                tvTestOnly.setText(response.body().toString());
//            }
//
//            @Override
//            public void onFailure(Call<Category> call, Throwable t) {
//
//            }
//        });

//        CREATE CATEGORY
//    Call<Category> call = apiService.createCategory(new Category("1", "1st_test", "111"));
//    call.enqueue(new Callback<Category>() {
//        @Override
//        public void onResponse(Call<Category> call, Response<Category> response) {
//            if (!response.isSuccessful()){
//                System.out.println("code: " + response.code());
//                return;
//            }
//
//            tvTestOnly.setText(response.body().toString());
//        }
//
//        @Override
//        public void onFailure(Call<Category> call, Throwable t) {
//
//        }
//    });

    }

    //---------------Set on item click listener to navigation bar------------------------------
    BottomNavigationView.OnNavigationItemSelectedListener navbarListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;

            switch (item.getItemId()) {
                case R.id.navbarManage:
                    selectedFragment = new ManageFragment();
                    toolbarTitle.setText("Manage");
                    break;
                case R.id.navbarStatistics:
                    selectedFragment = new StatisticsFragment();
                    toolbarTitle.setText("Statistics");
                    break;
                case R.id.navbarAdminLog:
                    selectedFragment = new AdminLogFragment();
                    toolbarTitle.setText("Admin log");
                    break;
            }

            assert selectedFragment != null;
            getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, selectedFragment).commit();

            return true;
        }
    };
}